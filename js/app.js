//app.js
define([
	'jquery',
	'underscore',
	'backbone',
	'handlebars',
	'text',

	'js/models/question',
	'js/collections/questions',

	'js/views/questionView',
	'js/views/questionListView',

	'text!tmpl/waitmessage.html'

	], function($, _, Backbone, handlebars, text,
		Question, Questions, 
		QuestionView, QuestionListView,
		reportTemplate, questionTemplate, counterTemplate, waitTemplate){
	
		var init = function(){	
			var questions = new Questions();

			var questionView = new QuestionView(questions);
			var questionListView = new QuestionListView(questions);
		};

		return {init: init};
	}
);