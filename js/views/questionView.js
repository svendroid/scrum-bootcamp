define([
	'jquery',
	'underscore',
	'backbone',
	'handlebars',
	'text',

    'js/models/question',

	'text!tmpl/question.html',
	'text!tmpl/counter.html'


], function ($, _, Backbone, Handlerbars, text, Question, questionTemplate, counterTemplate) {


	var QuestionView = Backbone.View.extend({
	    	el: '.content',

	    	initialize:function(questions){
				this.questions = questions;
				_.bindAll(this, "fetched");
				_.bindAll(this, 'onKeyDown');
				$(document).bind('keydown.nextprev', this.onKeyDown);
				
				this.listenTo(questions, 'reset', this.render);
				this.listenTo(questions, 'change', this.render);
				questions.fetch({success: this.fetched});

			},

			events :{
				'click #next' : 'next',
				'click #prev' : 'prev',
				'click #finish' : 'saveUsersChoice',
				'click #quickToReport' : 'saveUsersChoice'
			},
			render: function(){
				var template = Handlebars.compile(questionTemplate);
				var html = template(this.questions.getCurrent().toJSON());
				
				var templateCounter = Handlebars.compile(counterTemplate);
				json = this.questions.toJSON();
				json.currentIndex = this.questions.currentIndex + 1;
				var htmlCounter = templateCounter(json);

				this.$el.html(htmlCounter);
				this.$el.append(html);

				console.log(this.el);
			},
			fetched: function(){
				this.questions.reset(this.questions.shuffle(), {silent:true});
				this.render();
			},
			onKeyDown: function(e){
				if(e.keyCode === 39){
					this.next();

				}else if(e.keyCode === 37){
					this.prev();
				}
			},
			next:function(){
				this.saveUsersChoice();
				this.questions.next();
	    		this.render();
			},
			prev:function(){
				this.questions.prev();
				this.render();
			},
			saveUsersChoice: function(){
				var checkboxes = this.$el.find('input');
	    		for (var i = 0; i < checkboxes.length; i++) {
	    			var answers = this.questions.getCurrent().get('answers');
	    			answers[i].userchoice =  $(checkboxes[i]).is(':checked');
	    			this.questions.getCurrent().set('answers', answers);
				}
			}
	    });

	return QuestionView;

});