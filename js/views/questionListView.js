define([
	'jquery',
	'underscore',
	'backbone',
	'handlebars',
	'text',

	'text!tmpl/report.html'

], function ($, _, Backbone, Handlerbars, text, reportTemplate) {

	var QuestionListView = Backbone.View.extend({
		el: '.content',

		events:{
			'click #finish' : 'finish',
			'click #quickToReport' : 'finish'
		},
		initialize:function(questions){
			_.bindAll(this, "render");
			this.questions = questions;
			this.listenTo(questions, 'reset', this.render);
			this.listenTo(questions, 'change', this.render);
		},
		render: function(){
			var template = Handlebars.compile(reportTemplate);
			var json = this.questions.toJSON();
			json.right = this.questions.right;
			json.wrong = this.questions.wrong;
			json.percentage = this.questions.percentage;
			json.passed = this.questions.passed;
			var html = template(json);
			this.$el.html(html);
		},
		finish: function(){
			$(document).unbind("keydown.nextprev");

			this.questions.evaluate();

			this.render();
		}
	});

	return QuestionListView;

});