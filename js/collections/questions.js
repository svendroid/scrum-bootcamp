define([
    'underscore', 'backbone', 'js/models/question'
], function (_, Backbone, Question) {

	var Questions = Backbone.Collection.extend({
	    	defaults: {
	    		model: Question
	    	},
	    	model:Question,
	    	url: 'data/questionsAll.json',
	    	currentIndex: 0,
	    	startTime: 0,
	    	initialize: function(options){
	    		this.on("error", this.error, this);
	    		this.setCurrentIndex(0);
	    	},

	    	error: function(model, response, options){
	    		console.log("ERROR");
	    		console.log(model);
	    		console.log(response);
	    		console.log(options);
	    	},
	    	setCurrentIndex: function(index){
			    if ( index > -1 && index < this.size() ){
			        this.currentIndex = index;
			       	this.at(this.currentIndex).set('isFirst', this.currentIndex === 0);
			       	this.at(this.currentIndex).set('isLast', this.currentIndex === this.size()-1);
			    }
			},
			getCurrent: function(){
				this.setCurrentIndex(this.currentIndex);  //fix to set isFirst on first element
				return this.at(this.currentIndex);
			},
			prev: function() {
			    this.setCurrentIndex(this.currentIndex -1);
			},
			next: function() {
			    this.setCurrentIndex(this.currentIndex +1);
			},
			evaluate: function(){
				//alle fragen durch gehen und schauen ob userchoice und correct übereinstimmen
				this.right = 0;
				this.wrong = 0;

				this.each(function(question){
					question.set('valid', true);

					_.each(question.get('answers'), function(answer){
						if((typeof answer.correct == 'undefined')){
							answer.correct = false;
						}
						if((typeof answer.userchoice == 'undefined')){
							answer.userchoice = false;
						}

						if(answer.correct !== answer.userchoice ){
							question.set('valid', false);
						}
					});

					if(question.get('valid')){
						this.right += 1;
					}else{
						this.wrong += 1;
					}

				});

				this.percentage = Math.round((this.right / this.length) * 100);
				this.passed = this.percentage > 85;
			}

	    });
	
	return Questions;
});