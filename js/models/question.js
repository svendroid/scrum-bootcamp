define([
    'underscore', 'backbone'
], function (_, Backbone) {
    var Question = Backbone.Model.extend({
    	defaults:{
    		"question":"leere Frage",
    		"isFirst":true,
    		"isLast":true
    	}

    });

    return Question;
 });